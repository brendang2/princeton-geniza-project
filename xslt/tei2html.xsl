<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:tei="http://www.tei-c.org/ns/1.0" version="1.0">

   <xsl:output method="html" doctype-system="http://www.w3.org/TR/html4/strict.dtd" doctype-public="-//W3C//DTD HTML 4.01//EN" indent="yes"/>
   <xsl:preserve-space elements="l"/>
   <xsl:template match="/">
     <html lang="en">
       <head>
        <meta charset="utf-8"/>
          <style>
        #transcription { width:100%; margin: 0 auto; }
	    .hebrew, .arabic { direction:rtl; }
                
        .number {
            color: green;
            display: inline-block;
            font-size: 0.9em;
            width: 30px;
        }
          </style>
       </head>
       <body>
     <xsl:apply-templates/>
       </body>
     </html>
   </xsl:template>

  <xsl:template match="tei:teiHeader"/>

   <xsl:template match="tei:text">
     <div id="transcription">
     <xsl:apply-templates/>
     </div>
   </xsl:template>

  <xsl:template match="tei:body">
    <xsl:apply-templates/>
  </xsl:template>
  
  <xsl:template match="//tei:l[not(@rend='col')]">
    <div class="hebrew">
            <xsl:if test="@n">
            <span class="number">
                    <xsl:value-of select="@n"/>&#160;</span>
            </xsl:if>
            <xsl:apply-templates/>
        </div> 
  </xsl:template>

  <xsl:template match="//tei:p">
    <p>
    <xsl:apply-templates/>
    </p>
  </xsl:template>



  <xsl:template match="//tei:desc">
    <div class="description">
            <xsl:apply-templates/>
        </div> 
  </xsl:template>
  
  <xsl:template match="//tei:l[@rend='col']"/>

  <xsl:template match="//tei:label">
    <div class="meta">
            <xsl:apply-templates/>
        </div> 
  </xsl:template>
  
      <xsl:template match="tei:span">
    <span style="direction:ltr">
            <xsl:apply-templates/>
        </span>
  </xsl:template>
  
  <xsl:template match="tei:hi[@rend='superscript']">
     <sup>
     <xsl:apply-templates/>
     </sup>
   </xsl:template>

  <xsl:template match="//tei:note">
    <div class="note">
            <xsl:apply-templates/>
        </div>
  </xsl:template>


  <xsl:template match="//tei:incident">
    <h1 style="font-size:10em;color:red;">!</h1>
  </xsl:template>



<xsl:template match="//tei:cb">
    <div class="hebrew-column">
      <xsl:variable name="numTextNodes" select="count(following::tei:l[@rend='col']) - count(following::tei:cb/following::tei:l[@rend='col'])"/>
        <xsl:for-each select="following::tei:l[position() &lt;= $numTextNodes]">
          
      <div class="hebrew">
                    <span class="number">
                        <xsl:value-of select="@n"/>
                    </span>
                    <xsl:value-of select="."/>
                </div>
          <!--<xsl:if test="not(following-sibling::cb)"><div style='clear:both'/></xsl:if>
          <xsl:value-of select="following-sibling::text()"/>-->
        </xsl:for-each>
    </div>
</xsl:template>
</xsl:stylesheet>
